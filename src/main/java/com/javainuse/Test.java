package com.javainuse;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Test {

	public static void main(String[] args) {
		BCryptPasswordEncoder encrypt = new BCryptPasswordEncoder();
		System.out.println(encrypt.encode("password"));
	}
}
