package com.javainuse.config;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.javainuse.exception.ErrorDetails;

@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
    	httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    	ErrorDetails response = new ErrorDetails(401, "Unauthorised");
        response.setErrorMessages("Unauthorised");
       
        PrintWriter write =  httpServletResponse.getWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(write, response);
        write.flush();
    }
}
