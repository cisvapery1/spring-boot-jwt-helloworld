package com.javainuse.exception;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonTypeIdResolver;
import com.fasterxml.jackson.databind.jsontype.impl.TypeIdResolverBase;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import javax.validation.ConstraintViolation;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ErrorDetails<T> {

	private int statusCode;
	private String message;
	private String errorMessages;
	private T data;

	public ErrorDetails(HttpStatus statusCode, String message, String errorMessages, T data) {
		super();
		this.statusCode = statusCode.value();
		this.message = message;
		this.errorMessages = errorMessages;
		this.data = data;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(String errorMessages) {
		this.errorMessages = errorMessages;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "ErrorDetails [statusCode=" + statusCode + ", message=" + message + ", errorMessages=" + errorMessages
				+ ", data=" + data + "]";
	}

	public ErrorDetails(int statusCode, String message) {
		super();
		this.statusCode = statusCode;
		this.message = message;
	}

}
